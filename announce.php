<?php
	require 'lib/bencode.php';
	require 'lib/track.php';

	$uid = (!empty($_GET['u'])) ? $_GET['u'] : false;

	$response = new BEncoded;
	$response->NewNode('dict', '/');

	$response->Set('warning message', "Your IP is {$_SERVER['REMOTE_ADDR']}");
	$response->Set('interval', 300);
	$response->Set('min interval', 120);
	$response->Set('complete', 0);
	$response->Set('incomplete', 0);

	if ($uid) {
		$response->Set('tracker id', $uid);

		if (uid_is_known($uid)) {
			track_hit($uid);
		}
	}
		
	echo $response->ToString(); 

?>