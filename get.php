<?php
	require 'lib/bencode.php';
	require 'lib/uid.php';
	require 'lib/track.php';

	$uid = get_uid();
	$tracker = (($_SERVER['HTTPS'] == 'on') ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}/announce.php?u=" . urlencode($uid);
	
	try {
		$torrent = new BEncoded;

		$torrent->NewNode('dict', '/');
		$torrent->Set('/announce', $tracker);
		$torrent->Set('/announce-list', array(array($tracker)));
		$torrent->Set('/comment', "The ID to track this torrent is {$uid}");
		$torrent->Set('/created by', 'BTIP');
		$torrent->Set('/creation date', time());

		$torrent->NewNode('dict', '/info');
		$torrent->Set('/info/piece length', 64);
		$torrent->Set('/info/pieces', sha1('BTIP', true));
		$torrent->Set('/info/name', '.BitTorrent IP');
		$torrent->Set('/info/length', 42);
		$torrent->Set('/info/private', 1);
	
		$data = $torrent->ToString();
		unset($torrent);

		allow_tracking($uid);
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=BTIP.torrent');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . strlen($data));
		echo $data;

	} catch (Exception $e) {
		var_dump($e);
	}

?>