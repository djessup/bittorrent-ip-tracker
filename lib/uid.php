<?php

function set_uid($uid) {
	if (preg_match('/^[a-z0-9]{40}$/', $uid)) {
		setcookie('u', $uid, time()+60*60*24*365*10);
		$_COOKIE['u'] = $uid;
		return true;
	}
	return false;
}

function get_uid($generate = true) {
	if (!empty($_COOKIE['u']) && preg_match('/^[a-z0-9]{40}$/', $_COOKIE['u'])) {
		return $_COOKIE['u'];
	}
	
	if ($generate) {
		return generate_uid();
	}
	
	return null;
}

function generate_uid() {
	$uid = sha1((time()*rand()).$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
	set_uid($uid);
	return $uid;
}

?>