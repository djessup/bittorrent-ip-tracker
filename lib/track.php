<?php

$db_path = dirname(__FILE__).'/../hits.sqlite';
$init = !file_exists($db_path);

$db = new PDO("sqlite:{$db_path}");
$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

if ($init) {
	try {

		$db->exec("CREATE TABLE hits (
			id INTEGER DEFAULT NULL PRIMARY KEY AUTOINCREMENT,
			uid TEXT DEFAULT NULL,
			ip TEXT DEFAULT NULL,
			user_agent TEXT DEFAULT NULL,
			seen NUMERIC DEFAULT NULL
		);");

		$db->exec("CREATE TABLE uids (
			uid TEXT DEFAULT NULL PRIMARY KEY,
			seen NUMERIC DEFAULT NULL
		);");

	} catch(Exception $e) {}
}

function allow_tracking($uid) {
	global $db;

	if (!uid_is_known($uid)) {
		$q = $db->prepare("INSERT INTO uids (uid, seen) VALUES (:uid, :seen)");
		$q->execute(array(
			"uid" => $uid,
			"seen" => time(),
		));
	}
}

function uid_is_known($uid) {
	global $db;

	$q = $db->prepare("SELECT COUNT(*) FROM uids where uid = :uid");
	$q->execute(array("uid" => $uid));
	return ($q->fetchColumn() > 0);
}

function track_hit($uid) {
	global $db;

	try {
		// check if this hit is already registered
		$q = $db->prepare("SELECT * FROM hits WHERE uid = :uid AND ip = :ip AND user_agent = :ua");
		$q->execute(array(
			"uid" => $uid,
			"ip" => $_SERVER['REMOTE_ADDR'],
			"ua" => $_SERVER['HTTP_USER_AGENT'],
		));
		$hit = $q->fetch(PDO::FETCH_ASSOC);

		if ($hit) {
			// update last seen on existing hit
			$q = $db->prepare("UPDATE hits SET seen = :seen WHERE id = :id");
			$q->execute(array(
				'id' => $hit['id'],
				'seen' => time(),
			));
		} else {
			// brand new hit
			$q = $db->prepare("INSERT INTO hits (uid, ip, user_agent, seen) VALUES (:uid, :ip, :ua, :seen)");
			$q->execute(array(
				"uid" => $uid,
				"ip" => $_SERVER['REMOTE_ADDR'],
				"ua" => $_SERVER['HTTP_USER_AGENT'],
				"seen" => time(),
			));
		}

		return true;
	} catch (Exception $e) {
		return false;
	}

}

function remove_hit($id) {
	global $db;
	
	$uid = get_uid();

	$q = $db->prepare("DELETE FROM hits WHERE uid = :uid and id = :id");
	$q->execute(array(
		"id" => $id,
		"uid" => $uid,
	));
}

function get_hits($uid) {
	global $db;

	$q = $db->prepare("SELECT * FROM hits where uid = :uid ORDER BY seen DESC");
	$q->execute(array("uid" => $uid));
	return $q->fetchAll(PDO::FETCH_ASSOC);
}

?>