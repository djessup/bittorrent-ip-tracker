<?php
    require 'lib/uid.php';
    require 'lib/track.php';

    if (isset($_GET['renew'])) {
        $old_uid = get_uid();
        generate_uid();
    }

    if (isset($_GET['d'])) {
        remove_hit((int)$_GET['d']);
        header('Location: /');
        exit();
    }

    $uid = get_uid();

    $hit_uid = (empty($_GET['u'])) ? $uid : $_GET['u'];

    if ($hit_uid !== $uid) {
        if (set_uid($hit_uid)) {
            $uid = $hit_uid;
        }
    }

    $hits = get_hits($uid);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BitTorrent IP Tracker</title>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/dashboard.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-xs" href="/">BitTorrent IP Tracker</a>
            <a class="navbar-brand visible-xs" href="/">BTIP Tracker</a>
          </div>
          <div class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" action="/" method="get">
            <a href="get.php" class="btn btn-success">Get tracking torrent</a>
              <input type="text" class="form-control" name="u" placeholder="Enter ID to search for hits..." size="50">
              <input type="submit" class="btn btn-primary" value="Go">
            </form>
          </div>
        </div>
      </div>

      <div class="container-fluid">
        <div class="row">
          <div class="main">
            <?php if (isset($old_uid)): ?>
            <div class="alert alert-warning alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Tracking ID changed!</strong> You have been issued with a new tracking ID. If you didn't mean to do that you can <a href="/?u=<?php echo $old_uid; ?>">undo</a>.
            </div>
            <?php endif; ?>
            <h1 class="page-header">Dashboard</h1>
            <!-- <div class="table-responsive"> -->
              <?php if (!empty($hits)): ?>
                  <table class="table table-striped">
                      <thead>
                          <tr>
                              <th>IP</th>
                              <th>User agent</th>
                              <th>Last seen</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($hits as $hit): ?>
                              <tr<?php if ($hit['ip'] == $_SERVER['REMOTE_ADDR']): ?> class="warning"<?php endif; ?>>
                                  <td><?php echo $hit['ip']; ?></td>
                                  <td><?php echo $hit['user_agent']; ?></td>
                                  <td><abbr class="timeago" title="<?php echo date(DateTime::ISO8601, $hit['seen']); ?>"><?php echo date(DateTime::RFC2822, $hit['seen']); ?></abbr></td>
                                  <td><a href="/?d=<?php echo $hit['id']; ?>" class="close" aria-hidden="true">&times;</button></td>
                              </tr>
                          <?php endforeach; ?>
                          <tr class="info">
                              <td colspan="4" class="text-center">Your browser IP is <?php echo $_SERVER['REMOTE_ADDR']; ?></td>
                          </tr>
                      </tbody>
                  </table>
              <?php else: ?>
                  <p>No hits recorded yet.<p>
              <?php endif; ?>

              <p class="text-center subtle">
                  Your current ID is <?php echo $uid; ?><br>
                  <a href="/?renew">Renew</a>
              </p>

            <!-- </div> -->
          </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="js/jquery.timeago.js"></script>
        <script>
          $(document).ready(function(){
            $('.timeago').timeago();
          });
        </script>
    </body>
</html>

